# Copyright (C) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/test.gni")
import("//foundation/filemanagement/dfs_service/distributedfile.gni")

ohos_unittest("cloud_sync_service_stub_test") {
  module_out_path = "filemanagement/dfs_service"

  sources = [
    "${services_path}/cloudsyncservice/src/ipc/cloud_sync_service_stub.cpp",
    "cloud_sync_service_stub_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include/ipc",
    "${services_path}/cloudsyncservice/include",
    "${distributedfile_path}/test/unittests/cloudsync_api/cloudsync_impl/include",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:dataobs_manager",
    "dfs_service:cloudsync_kit_inner",
    "hilog:libhilog",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = [ "private=public" ]

  use_exceptions = true
}

ohos_unittest("cloud_sync_service_test") {
  module_out_path = "filemanagement/dfs_service"

  sources = [
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_error.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_record_field.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/drive_kit.cpp",
    "${distributedfile_path}/frameworks/native/cloudsync_kit_inner/src/cloud_sync_common.cpp",
    "${distributedfile_path}/test/unittests/cloudsync_sa/mock/battery_status_mock.cpp",
    "${distributedfile_path}/test/unittests/cloudsync_sa/mock/cloud_status_mock.cpp",
    "${distributedfile_path}/test/unittests/cloudsync_sa/mock/network_status_mock.cpp",
    "${distributedfile_path}/test/unittests/cloudsync_sa/mock/sdk_helper_mock.cpp",
    "${media_library_path}/frameworks/innerkitsimpl/media_library_helper/src/base_column.cpp",
    "${media_library_path}/frameworks/innerkitsimpl/media_library_helper/src/media_column.cpp",
    "${media_library_path}/frameworks/innerkitsimpl/media_library_helper/src/photo_album_column.cpp",
    "${media_library_path}/frameworks/innerkitsimpl/media_library_helper/src/photo_map_column.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/data_handler.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/data_sync_const.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/data_sync_manager.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/data_sync_notifier.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/data_syncer.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/gallery_data_sync/album_data_handler.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/gallery_data_sync/data_convertor.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/gallery_data_sync/file_data_convertor.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/gallery_data_sync/file_data_handler.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/gallery_data_sync/gallery_data_syncer.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/rdb_data_handler.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/sync_state_manager.cpp",
    "${services_path}/cloudsyncservice/src/data_sync/task.cpp",
    "${services_path}/cloudsyncservice/src/ipc/cloud_download_callback_manager.cpp",
    "${services_path}/cloudsyncservice/src/ipc/cloud_sync_callback_manager.cpp",
    "${services_path}/cloudsyncservice/src/ipc/cloud_sync_callback_proxy.cpp",
    "${services_path}/cloudsyncservice/src/ipc/cloud_sync_service.cpp",
    "${services_path}/cloudsyncservice/src/ipc/cloud_sync_service_stub.cpp",
    "${services_path}/cloudsyncservice/src/sync_rule/battery_status_listener.cpp",
    "${services_path}/cloudsyncservice/src/sync_rule/net_conn_callback_observer.cpp",
    "cloud_sync_service_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include/ipc",
    "${services_path}/cloudsyncservice/include",
    "${services_path}/cloudsyncservice/include/data_sync",
    "${services_path}/cloudsyncservice/include/data_sync/gallery_data_sync",
    "${distributedfile_path}/test/unittests/cloudsync_api/cloudsync_impl/include",
    "${media_library_path}/interfaces/inner_api/media_library_helper/include",
    "${media_library_path}/frameworks/services/media_thumbnail/include",
    "${media_library_path}/frameworks/utils/include",
    "${distributedfile_path}/adapter/cloud_adapter_example/include",
  ]

  deps = [
    "${utils_path}:libdistributedfiledentry",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:dataobs_manager",
    "common_event_service:cesfwk_innerkits",
    "dfs_service:cloudsync_kit_inner",
    "hilog:libhilog",
    "init:libbegetutil",
    "ipc:ipc_core",
    "netmanager_base:net_conn_manager_if",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = [
    "private=public",
    "LOG_DOMAIN=0xD004307",
    "LOG_TAG=\"CLOUDSYNC_TEST\"",
  ]

  use_exceptions = true
}

ohos_unittest("cloud_sync_callback_proxy_test") {
  module_out_path = "filemanagement/dfs_service"

  sources = [
    "${services_path}/cloudsyncservice/src/ipc/cloud_sync_callback_proxy.cpp",
    "cloudsync_callback_proxy_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include",
    "${services_path}/cloudsyncservice/include/ipc",
    "${distributedfile_path}/test/unittests/cloudsync_api/cloudsync_impl/include",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:dataobs_manager",
    "dfs_service:cloudsync_kit_inner",
    "hilog:libhilog",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = [ "private=public" ]

  use_exceptions = true
}

ohos_unittest("cloud_sync_callback_manager_test") {
  module_out_path = "filemanagement/dfs_service"

  sources = [ "cloudsync_callback_manager_test.cpp" ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include/ipc",
    "${distributedfile_path}/test/unittests/cloudsync_api/cloudsync_impl/include",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:dataobs_manager",
    "dfs_service:cloudsync_kit_inner",
    "hilog:libhilog",
    "ipc:ipc_core",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = [
    "private=public",
    "protected=public",
  ]

  use_exceptions = true
}

ohos_unittest("cloud_download_callback_manager_test") {
  module_out_path = "filemanagement/dfs_service"

  sources = [
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_container.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_database.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_error.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/dk_record_field.cpp",
    "${distributedfile_path}/adapter/cloud_adapter_example/src/drive_kit.cpp",
    "cloud_download_callback_manager_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include/ipc",
    "${services_path}/cloudsyncservice/src/ipc",
    "${distributedfile_path}/test/unittests/cloudsync_api/cloudsync_impl/include",
    "${distributedfile_path}/test/unittests/cloudsync_sa/data_sync",
    "${distributedfile_path}/frameworks/native/cloudsync_kit_inner/include",
    "${distributedfile_path}/adapter/cloud_adapter_example/include",
    "${distributedfile_path}/services/cloudsyncservice/include/data_sync",

    "${services_path}/cloudsyncservice/include",
    "${services_path}/cloudsyncservice/include/data_sync",
    "${services_path}/cloudsyncservice/include/data_sync/gallery_data_sync",
    "${distributedfile_path}/interfaces/inner_api/native/cloudsync_kit_inner",
    "${media_library_path}/interfaces/inner_api/media_library_helper/include",
    "${media_library_path}/frameworks/services/media_thumbnail/include",
    "${media_library_path}/frameworks/utils/include",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa",
    "${utils_path}:libdistributedfiledentry",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:dataobs_manager",
    "dfs_service:cloudsync_kit_inner",
    "hilog:libhilog",
    "init:libbegetutil",
    "ipc:ipc_core",
    "netmanager_base:net_conn_manager_if",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = [
    "private=public",
    "protected=public",

    "LOG_DOMAIN=0xD004310",
    "LOG_TAG=\"DATA_SYNC_TEST\"",
  ]

  use_exceptions = true
}

ohos_unittest("cloud_download_callback_proxy_test") {
  module_out_path = "filemanagement/dfs_service"

  sources = [
    "${services_path}/cloudsyncservice/src/ipc/cloud_download_callback_proxy.cpp",
    "cloud_download_callback_proxy_test.cpp",
  ]

  include_dirs = [
    "${services_path}/cloudsyncservice/include",
    "${services_path}/cloudsyncservice/include/ipc",
    "${distributedfile_path}/test/unittests/cloudsync_api/cloudsync_impl/include",
  ]

  deps = [
    "${services_path}/cloudsyncservice:cloudsync_sa",
    "${utils_path}:libdistributedfiledentry",
    "${utils_path}:libdistributedfileutils",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
  ]

  external_deps = [
    "ability_base:want",
    "dfs_service:cloudsync_kit_inner",
    "hilog:libhilog",
    "init:libbegetutil",
    "ipc:ipc_core",
    "netmanager_base:net_conn_manager_if",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = [ "private=public" ]

  use_exceptions = true
}

group("cloudsync_sa_ipc_test") {
  testonly = true
  deps = [
    ":cloud_download_callback_proxy_test",
    ":cloud_sync_callback_proxy_test",
    ":cloud_sync_service_stub_test",
  ]
}
